import React from 'react'
import {Router, Route} from 'react-router'
import Frame from 'modules/layouts/frame'
import Intl from 'i18n/intl'

const routes = history => (
  <Router history={history}>
    <Route component={Intl}>
      <Route path='/' component={Frame} />
    </Route>
  </Router>
)

export default routes
