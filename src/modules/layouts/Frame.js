import React, {Component} from 'react'
import Main from 'modules/containers/Main/Main'
import Header from 'modules/components/Header/Header'

export default class Frame extends Component {
  render() {
    return (
      <div style={{height: '100%'}}>
        <Header />
        <Main />
      </div>
    )
  }
}
