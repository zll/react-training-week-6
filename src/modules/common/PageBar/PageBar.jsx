import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './PageBar.css'

@CSSModules(styles, {allowMultiple: true})

export default class PageBar extends Component {
  static propTypes = {
    total: React.PropTypes.number,
    currentPage: React.PropTypes.number,
    pageSize: React.PropTypes.number
    // onChange: React.PropTypes.func
  }

  state = {
    totalPage: 0,
    pageSize: this.props.pageSize
  }

  componentWillReceiveProps(props) {
    const pageSize = props.pageSize || 8
    const totalPage = Math.ceil(props.total / pageSize)
    this.setState({
      totalPage: totalPage,
      pageSize: pageSize
    })
  }

  getPrevEl = () => {
    if (this.props.currentPage > 1) {
      return (<li>&lt;</li>)
    } else {
      return (<li styleName="disable">&lt;</li>)
    }
  }

  getNextEl = () => {
    if (this.props.currentPage < this.state.totalPage) {
      return (<li styleName="page-next">&gt;</li>)
    } else {
      return (<li styleName="page-next disable">&gt;</li>)
    }
  }

  getPages = () => {
    const totalPage = this.state.totalPage
    const currentPage = this.props.currentPage
    let pages = []

    let isEllipsis = false
    for (let i = 1; i <= totalPage; i++) {
      if (totalPage < 6) {
        pages.push(<li data-page={i} styleName={currentPage === i ? 'page-select' : ''} key={i} onClick={this.onPageClick}>{i}</li>)
      } else {
        if (currentPage < 4 || currentPage > totalPage - 2) {
          if (i < 4 || i > totalPage - 2) {
            pages.push(<li data-page={i} styleName={currentPage === i ? 'page-select' : ''} key={i} onClick={this.onPageClick}>{i}</li>)
            isEllipsis = true
          } else {
            if (isEllipsis) {
              pages.push(<li data-page="elli" styleName="page-elli" key={i}>{'...'}</li>)
              isEllipsis = false
            }
          }
        } else {
          if (i < 2 || i > (totalPage - 1) || (i > (currentPage - 2) && i < (currentPage + 2))) {
            pages.push(<li data-page={i} styleName={currentPage === i ? 'page-select' : ''} key={i} onClick={this.onPageClick}>{i}</li>)
            isEllipsis = true
          } else {
            if (isEllipsis) {
              pages.push(<li data-page="elli" styleName="page-elli" key={i}>{'...'}</li>)
              isEllipsis = false
            }
          }
        }
      }
    }
    return pages
  }

  render() {
    if (this.props.total === 0) return null
    return (
      <ul styleName='pagebar'>
        {this.getPrevEl()}
        {this.getPages()}
        {this.getNextEl()}
      </ul>
    )
  }
}
