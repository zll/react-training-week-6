import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './CourseTool.css'

@CSSModules(styles, {allowMultiple: true})

export default class CourseTool extends Component {
  static propTypes = {
    type: React.PropTypes.string, // 'edit' or 'delete' or 'play'
    text: React.PropTypes.string
  }

  state = {
    played: false
  }

  render() {
    return (
      <div
        styleName={`course-tool course-${this.props.type} ${this.state.played ? 'course-play-played' : ''}`}>
        <i />
        <span>{this.props.text}</span>
      </div>
    )
  }
}
