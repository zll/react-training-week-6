import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './Dialog.css'

@CSSModules(styles, {allowMultiple: true})

export default class Mask extends Component {
  static propTypes = {
    title: React.PropTypes.any,
    content: React.PropTypes.any,
    footer: React.PropTypes.any,
    style: React.PropTypes.object,
    width: React.PropTypes.string,
    height: React.PropTypes.string,
    onCloseClick: React.PropTypes.func
  }

  static defaultProps = {
    width: '750px',
    height: '580px',
    onCloseClick: () => {}
  }
  render() {
    return (
      <div
        styleName="dialog"
        style={{
          ...this.props.style,
          width: this.props.width,
          height: this.props.height
        }}>
        <div styleName="dialog-title">
          <i styleName="dialog-close" onClick={this.props.onCloseClick} />
          {this.props.title}
        </div>
        <div styleName="dialog-content">
          {this.props.content}
        </div>
        <div styleName="dialog-footer">
          {this.props.footer}
        </div>
      </div>
    )
  }
}
