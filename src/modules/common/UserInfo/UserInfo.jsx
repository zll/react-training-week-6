import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './UserInfo.css'

@CSSModules(styles, {allowMultiple: true})

export default class UserInfo extends Component {
  static propTypes = {
    label: React.PropTypes.string.isRequired,
    content: React.PropTypes.string.isRequired,
    img: React.PropTypes.string.isRequired
  }
  render() {
    return (
      <div styleName="user-info">
        <img src={this.props.img} alt="user" />
        <span styleName="user-item">{this.props.label}：</span>
        <span>{this.props.content}</span>
      </div>
    )
  }
}
