import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './Mask.css'

@CSSModules(styles, {allowMultiple: true})

export default class Mask extends Component {
  static propTypes = {
    children: React.PropTypes.node,
    show: React.PropTypes.bool
  }

  render() {
    return (
      <div
        styleName={`dialog-mask ${this.props.show ? 'mask-show' : 'mask-hide'}`}>
        {this.props.children}
      </div>
    )
  }
}
