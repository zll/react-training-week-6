import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './List.css'

@CSSModules(styles, {allowMultiple: true})

export default class List extends Component {
  static propTypes = {
    course: React.PropTypes.object,
    selected: React.PropTypes.any,
    onClick: React.PropTypes.func
  }
  render() {
    return (
      <li
        styleName={'course-list' + (this.props.selected === this.props.course.id ? ' course-select' : '')}
        onClick={this.props.onClick}>
        <i />
        <span>{this.props.course.name}</span>
      </li>
    )
  }
}
