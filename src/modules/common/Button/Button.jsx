import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './Button.css'

@CSSModules(styles, {allowMultiple: true})

export default class Button extends Component {
  static propTypes = {
    text: React.PropTypes.string,
    style: React.PropTypes.object,
    width: React.PropTypes.string,
    height: React.PropTypes.string,
    icon: React.PropTypes.string,
    type: React.PropTypes.string,
    onClick: React.PropTypes.func
  }

  static defaultProps = {
    width: '120px',
    height: '34px',
    type: ''
  }
  render() {
    return (
      <div
        styleName={`btn ${this.props.type}`}
        style={{
          ...this.props.style,
          width: this.props.width,
          height: this.props.height
        }}
        onClick={this.props.onClick}>
        {this.props.icon && <i styleName={`btn-${this.props.icon} btn-icon`} />}
        {this.props.text}
      </div>
    )
  }
}
