import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './SearchInput.css'

@CSSModules(styles, {allowMultiple: true})

export default class SearchInput extends Component {
  static propTypes = {
    style: React.PropTypes.object,
    width: React.PropTypes.string,
    height: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    onChange: React.PropTypes.func
  }

  static defaultProps = {
    onChange: () => {},
    width: '310px',
    height: '36px'
  }

  handleChange = (e) => {
    const value = e.target.value
    this.props.onChange(value)
  }

  render() {
    return (
      <div
        styleName="search-input"
        style={this.props.style}>
        <div
          styleName="search-input-container"
          style={{width: this.props.width, height: this.props.height}}>
          <i />
          <input
            type="text"
            placeholder={this.props.placeholder}
            onChange={this.handleChange} />
        </div>
      </div>
    )
  }
}
