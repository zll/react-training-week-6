import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './TabSwitch.css'

@CSSModules(styles, {allowMultiple: true})

export default class TabSwitch extends Component {
  static propTypes = {
    tabs: React.PropTypes.arrayOf(
      React.PropTypes.object
    ),
    onClick: React.PropTypes.func
  }

  static defaultProps = {
    onClick: () => {}
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: 1
    }
  }

  handleClick = (id) => {
    this.setState({
      selected: id
    })
    this.props.onClick(id)
  }

  render() {
    return (
      <ul styleName="tab-switch">
        {this.props.tabs.map((tab, index) => (
          <li
            key={tab.id}
            styleName={(index % 2 === 0 ? 'tab-odd' : '') +
              (this.state.selected === tab.id ? ' tab-select' : '')}
            onClick={() => { this.handleClick(tab.id) }}><span>{tab.text}</span></li>
        ))}
      </ul>
    )
  }
}
