import React from 'react'
import {connect} from 'react-redux'
import CourseBox from 'modules/components/CourseBox/CourseBox'
import {coursesGet, classCoursesGet} from 'modules/actions'

const CourseContainer = props => (
  <CourseBox {...props} />
)

const mapStateToProps = state => ({
  courses: state.courses.items,
  classCourses: state.classCourses.items
})

export default connect(
  mapStateToProps,
  {coursesGet, classCoursesGet}
)(CourseContainer)
