import {handleActions} from 'redux-actions'
import * as types from 'modules/constants/actionTypes'

const initialState = {
  loading: true,
  error: false,
  items: []
}

const initialClassCourses = {
  loading: true,
  error: false,
  items: []
}

export const courses = handleActions({
  [`${types.COURSES_GET}_PENDING`](state, action) {
    return {
      ...state,
      loading: true,
      error: false
    }
  },
  [types.COURSES_GET]: {
    next(state, action) {
      return {
        ...state,
        loading: false,
        error: false,
        items: action.payload.data
      }
    }
  },
  throw(state, action) {
    return {
      ...state,
      loading: false,
      error: true
    }
  }
}, initialState)

export const classCourses = handleActions({
  [`${types.CLASSCOURSES_GET}_PENDING`](state, action) {
    return {
      ...state,
      loading: true,
      error: false
    }
  },
  [types.CLASSCOURSES_GET]: {
    next(state, action) {
      return {
        ...state,
        loading: false,
        error: false,
        items: action.payload.data
      }
    }
  },
  throw(state, action) {
    return {
      ...state,
      loading: false,
      error: true
    }
  }
}, initialClassCourses)
