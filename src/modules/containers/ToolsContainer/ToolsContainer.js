import React from 'react'
import {connect} from 'react-redux'
import ToolsBar from 'modules/components/ToolsBar/ToolsBar'
import {membersGet} from 'modules/actions/'

const ToolsContainer = (props) => (
  <ToolsBar {...props} />
)

const mapStateToProps = state => ({
  members: state.members.items
})

export default connect(
  mapStateToProps,
  {membersGet}
)(ToolsContainer)
