import {handleActions} from 'redux-actions'
import * as types from 'modules/constants/actionTypes'

const initialState = {
  loading: true,
  error: false,
  items: []
}

export const members = handleActions({
  [`${types.MEMBERS_GET}_PENDING`](state, action) {
    return {
      ...state,
      loading: true,
      error: false
    }
  },
  [types.MEMBERS_GET]: {
    next(state, action) {
      return {
        ...state,
        loading: false,
        error: false,
        items: action.payload.data
      }
    }
  },
  throw(state, action) {
    return {
      ...state,
      loading: false,
      error: true
    }
  }
}, initialState)
