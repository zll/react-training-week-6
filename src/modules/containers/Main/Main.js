import React from 'react'
import {connect} from 'react-redux'
import MainContent from 'modules/components/MainContent/MainContent'
import {courseMenuGet} from 'modules/actions/'

const Main = (props) => (
  <MainContent {...props} />
)

const mapStateToProps = state => ({
  courseMenu: state.courseMenu.items
})

export default connect(
  mapStateToProps,
  {courseMenuGet}
)(Main)
