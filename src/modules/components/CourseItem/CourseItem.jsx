import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './CourseItem.css'
import CourseTool from 'modules/common/CourseTool/CourseTool'

@CSSModules(styles, {allowMultiple: true})

export default class CourseItem extends Component {
  static propTypes = {
    style: React.PropTypes.object,
    course: React.PropTypes.object
  }

  getFormatTime = (timestamp) => {
    const date = new Date(timestamp)
    let month = date.getMonth() + 1
    let day = date.getDate()
    let min = date.getMinutes()
    const zeroFill = (num) => (num > 9 ? num : '0' + num)

    return `${date.getFullYear()}/${zeroFill(month)}/${zeroFill(day)}` +
      ` ${date.getHours()}:${zeroFill(min)}`
  }

  render() {
    const course = this.props.course
    return (
      <div styleName="course-card" style={this.props.style}>
        <div styleName="course-banner">
          <div styleName="course-info">
            <img src={course.pic} alt="bg" />
            <div data-title={course.author} styleName="course-meta">
              <i />
              <span styleName="course-meta-friends">{course.author}</span>
              <span styleName="course-meta-time">{this.getFormatTime(course.datetime)}</span>
            </div>
          </div>
          <div styleName="course-tools-bar">
            <CourseTool type="edit" text="编辑" />
            <CourseTool type="play" text="播放" />
            <CourseTool type="delete" text="删除" />
          </div>
        </div>
        <p styleName="course-name">{course.name}</p>
      </div>
    )
  }
}
