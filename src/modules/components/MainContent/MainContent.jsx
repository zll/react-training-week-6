import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './MainContent.css'
import Aside from '../Aside/Aside'
import CoursesContent from '../CoursesContent/CoursesContent'

@CSSModules(styles, {allowMultiple: true})

export default class MainContent extends Component {
  static propTypes = {
    courseMenu: React.PropTypes.array,
    courseMenuGet: React.PropTypes.func
  }

  componentDidMount() {
    this.props.courseMenuGet()
  }

  render() {
    return (
      <div styleName="main">
        <Aside courseMenu={this.props.courseMenu} style={{float: 'left'}} />
        <CoursesContent />
      </div>
    )
  }
}
