import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './SearchBar.css'
import Button from 'modules/common/Button/Button'
import SearchInput from 'modules/common/SearchInput/SearchInput'

@CSSModules(styles, {allowMultiple: true})

export default class SearchBar extends Component {
  render() {
    return (
      <div styleName="search-bar">
        <Button
          width="75px"
          style={{float: 'right'}}
          text="搜索" />
        <SearchInput
          style={{float: 'right', marginRight: '10px'}}
          placeholder="搜索资源..." />
      </div>
    )
  }
}
