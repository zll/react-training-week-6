import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './CourseBox.css'
import CourseItem from '../CourseItem/CourseItem'
import PageBar from 'modules/common/pageBar/pageBar'

@CSSModules(styles, {allowMultiple: true})

export default class CourseBox extends Component {
  static propTypes = {
    tab: React.PropTypes.number,
    // courses: React.PropTypes.array,
    // classCourses: React.PropTypes.array,
    coursesGet: React.PropTypes.func,
    classCoursesGet: React.PropTypes.func
  }

  state = {
    tab: 1,
    currentPage: 1,
    pageSize: 8,
    dataList: []
  }

  getData = (id) => {
    switch (id) {
      case 1:
        this.props.coursesGet()
        break
      case 2:
        this.props.classCoursesGet()
        break
    }
  }

  getDataList = (props) => {
    switch (this.state.tab) {
      case 1:
        this.setState({dataList: props.courses})
        break
      case 2:
        this.setState({dataList: props.classCourses})
        break
    }
  }

  componentDidMount() {
    this.getData(this.state.tab)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.tab !== nextProps.tab) {
      this.getData(nextProps.tab)
      this.setState({tab: nextProps.tab})
    }
    this.getDataList(nextProps)
  }

  render() {
    return (
      <div
        styleName={`course-box ${this.state.dataList.length > 0 ? '' : 'empty-course-box'}`}>
        {this.state.dataList.map((data) => (
          <CourseItem
            key={data.id}
            style={{margin: '0 44px 44px 0'}}
            course={data} />
        ))}
        <div styleName="page">
          <PageBar
            total={this.state.dataList.length}
            currentPage={this.state.currentPage}
            pageSize={this.state.pageSize} />
        </div>
      </div>
    )
  }
}
