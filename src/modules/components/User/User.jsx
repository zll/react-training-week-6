import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './User.css'
import UserInfo from 'modules/common/UserInfo/UserInfo'

@CSSModules(styles, {allowMultiple: true})

export default class User extends Component {
  render() {
    return (
      <div styleName="user">
        <ul>
          <li styleName="li-space">
            <UserInfo label="用户" content="王小明" img="src/static/images/user.png" />
          </li>
          <li>
            <UserInfo label="身份" content="学生" img="src/static/images/id_card.png" />
          </li>
        </ul>
      </div>
    )
  }
}
