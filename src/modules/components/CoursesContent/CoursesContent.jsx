import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './CoursesContent.css'
import SearchBar from '../SearchBar/SearchBar'
import CourseContainer from 'modules/containers/CourseContainer/CourseContainer'
import ToolsContainer from 'modules/containers/ToolsContainer/ToolsContainer'

@CSSModules(styles, {allowMultiple: true})

export default class CoursesContent extends Component {
  state = {
    tabId: 1
  }
  handleTabChange = (id) => {
    this.setState({
      tabId: id
    })
  }
  render() {
    return (
      <div styleName="courses-content">
        <ToolsContainer tabChange={this.handleTabChange} />
        <SearchBar />
        <CourseContainer tab={this.state.tabId} />
      </div>
    )
  }
}
