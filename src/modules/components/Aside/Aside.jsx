import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './Aside.css'
import CourseList from '../CourseList/CourseList'

@CSSModules(styles, {allowMultiple: true})

export default class Aside extends Component {
  static propTypes = {
    courseMenu: React.PropTypes.array,
    style: React.PropTypes.object
  }
  render() {
    return (
      <div styleName="aside" style={this.props.style}>
        <div styleName="course">课程列表</div>
        <CourseList courseMenu={this.props.courseMenu} />
      </div>
    )
  }
}
