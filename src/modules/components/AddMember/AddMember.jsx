import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './AddMember.css'
import Dialog from 'modules/common/Dialog/Dialog'
import Button from 'modules/common/Button/Button'
import SearchInput from 'modules/common/SearchInput/SearchInput'

@CSSModules(styles, {allowMultiple: true})

class Content extends Component {
  static propTypes = {
    members: React.PropTypes.array,
    sendSelct: React.PropTypes.func
  }

  state = {
    selected: {id: -1, name: ''},
    delMember: {id: -1, name: ''},
    saved: []
  }

  componentWillReceiveProps(prevProps) {
    if (prevProps.confirmSelect) {
      this.props.sendSelct(this.state.saved)
      this.setState({
        selected: {id: -1, name: ''},
        delMember: {id: -1, name: ''},
        saved: []
      })
    }
    this.setState({saved: prevProps.savedMembers})
  }

  handleSaved = () => {
    if (this.state.selected.id === -1) { return }
    let existFlag = false
    const saved = this.state.saved
    const selected = this.state.selected
    if (saved.length === 0) {
      saved.push(selected)
      this.setState({selected: {id: -1, name: ''}})
    } else {
      for (let v of saved) {
        if (v.id === selected.id) {
          existFlag = true
        }
      }
      if (!existFlag) {
        saved.push(selected)
        this.setState({selected: {id: -1, name: ''}})
      }
    }
  }

  handleDel = () => {
    if (this.state.delMember.id === -1) return
    const saved = [...this.state.saved]
    for (const item of saved.entries()) {
      if (item[1].id === this.state.delMember.id) {
        saved.splice(item[0], 1)
        this.setState({saved})
      }
    }
    this.setState({delMember: {id: -1, name: ''}})
  }

  btnStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginLeft: '-30px'
  }
  render() {
    console.log()
    return (
      <div styleName="class-member-content">
        <div styleName="member-saved">
          <ul styleName="member-name scrollbar">
            {this.state.saved.map((item) => (
              <li
                key={item.id}
                data-id={item.id}
                styleName={this.state.delMember.id === item.id ? 'member-select' : ''}
                onClick={() => { this.setState({delMember: item}) }}
                >{item.name}</li>
            ))}
          </ul>
        </div>
        <div styleName="member-list">
          <SearchInput
            style={{
              border: 'none',
              borderBottom: '1px solid rgb(208, 208, 208)'
            }}
            width="138px"
            placeholder="搜索人名..." />
          <ul styleName="member-name scrollbar">
            {this.props.members.map((member) => (
              <li
                key={member.id}
                styleName={this.state.selected.id === member.id ? 'member-select' : ''}
                data-id={member.id}
                onClick={() => { this.setState({selected: member}) }}>{member.name}</li>
            ))}
          </ul>
        </div>
        <Button
          text="确定"
          width='60px'
          height="36px"
          style={{
            ...this.btnStyle,
            marginTop: '-54px'
          }}
          onClick={this.handleSaved} />
        <Button
          text="删除"
          width='60px'
          height="36px"
          style={{
            ...this.btnStyle,
            marginTop: '18px'
          }}
          type="gray"
          onClick={this.handleDel} />
      </div>
    )
  }
}

export default class AddMember extends Component {
  static propTypes = {
    members: React.PropTypes.array,
    savedMembers: React.PropTypes.array,
    onCloseClick: React.PropTypes.func,
    getSaved: React.PropTypes.func
  }

  state = {
    confirmSelect: false
  }

  handleConfirm = (arr) => {
    this.props.getSaved(arr)
    this.setState({confirmSelect: false})
    this.props.onCloseClick()
  }

  render() {
    return (
      <Dialog
        title="班级成员"
        width="425px"
        style={{top: '-110px', left: '-280px'}}
        content={<Content
          members={this.props.members}
          savedMembers={this.props.savedMembers}
          confirmSelect={this.state.confirmSelect}
          sendSelct={this.handleConfirm} />}
        footer={<Button
          text="确定"
          width="75px"
          style={{float: 'right', marginRight: '60px'}}
          onClick={() => { this.setState({confirmSelect: true}) }} />}
        onCloseClick={this.props.onCloseClick} />
    )
  }
}
