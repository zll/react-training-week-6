import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './Header.css'
import User from '../User/User'

@CSSModules(styles, {allowMultiple: true})

export default class Header extends Component {
  render() {
    return (
      <div styleName="header">
        <div styleName="logo">
          <img src="src/static/images/101_logo.png" alt="101_logo" />
          <h1>VR创客教室</h1>
        </div>
        <div styleName="header-content">
          <User />
          <select>
            <option value="设备选择">设备选择</option>
          </select>
        </div>
      </div>
    )
  }
}
