import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './AddCourse.css'
import Dialog from 'modules/common/Dialog/Dialog'
import AddCourseContent from './AddCourseContent'
import Button from 'modules/common/Button/Button'

@CSSModules(styles, {allowMultiple: true})

class Footer extends Component {
  render() {
    return (
      <div>
        <Button
          text="确定提交"
          width="120px"
          style={{margin: '0 auto'}} />
        <span styleName="add-course-footer">提交后显示在班级作品区</span>
      </div>
    )
  }
}

export default class AddCourse extends Component {
  static propTypes = {
    onCloseClick: React.PropTypes.func,
    onOpenMember: React.PropTypes.func,
    saved: React.PropTypes.array,
    delMember: React.PropTypes.func
  }

  render() {
    return (
      <Dialog
        title="新建作品"
        content={<AddCourseContent
          saved={this.props.saved}
          onOpenMember={this.props.onOpenMember}
          delMember={this.props.delMember} />}
        footer={<Footer />}
        onCloseClick={this.props.onCloseClick}
      />
    )
  }
}
