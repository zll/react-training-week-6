import React, {Component} from 'react'
import CSSModules from 'react-css-modules'
import styles from './AddCourseContent.css'

@CSSModules(styles, {allowMultiple: true})

export default class AddCourseContent extends Component {
  static propTypes = {
    onOpenMember: React.PropTypes.func,
    saved: React.PropTypes.array,
    delMember: React.PropTypes.func
  }
  render() {
    return (
      <div styleName="add-course-container">
        <label>资源名称</label>
        <input styleName='resource-name' type="text" />
        <div styleName="author">
          <label>作者</label>
          <ul styleName="author-name scrollbar">
            {this.props.saved.map((item, index) => (
              <li
                key={item.id}
                data-id={item.id}>
                {item.name}
                <i
                  styleName="btn-del-author"
                  onClick={() => { this.props.delMember(index) }} />
              </li>
            ))}
          </ul>
          <img onClick={this.props.onOpenMember} src="src/static/images/add_author.png" alt="add_author" />
        </div>
        <label>配图</label>
        <input styleName="pic-upload" readOnly value="点击上传图片" type="text" />
        <label>简介</label>
        <textarea styleName="abstract scrollbar">{}</textarea>
      </div>
    )
  }
}
