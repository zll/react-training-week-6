import React, {Component, PropTypes} from 'react'
import CSSModules from 'react-css-modules'
import styles from './ToolsBar.css'
import TabSwitch from 'modules/common/TabSwitch/TabSwitch'
import Button from 'modules/common/Button/Button'
import Mask from 'modules/common/Mask/Mask'
import AddCourse from '../AddCourse/AddCourse'
import AddMember from '../AddMember/AddMember'

@CSSModules(styles, {allowMultiple: true})

export default class ToolsBar extends Component {
  static propTypes = {
    tabChange: PropTypes.func,
    members: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    })),
    membersGet: PropTypes.func
  }

  state = {
    showCourse: false,
    showMember: false,
    savedMembers: []
  }

  componentDidMount() {
    this.props.membersGet()
  }

  handleTabClick = (id) => {
    this.props.tabChange(id)
  }

  handleCloseMember = () => {
    this.setState({showMember: false})
  }

  handleCloseCourse = () => {
    this.setState({showCourse: false})
  }

  handleOpenMember = () => {
    this.setState({showMember: true})
  }

  handleDelMember = (index) => {
    const members = [...this.state.savedMembers]
    members.splice(index, 1)
    this.setState({savedMembers: members})
  }

  handleAddClick = () => {
    this.props.membersGet()
    this.setState({showCourse: true})
  }

  getSaved = (saved) => {
    this.setState({savedMembers: saved})
  }

  render() {
    const tabs = [
      {id: 1, text: '我的作品'},
      {id: 2, text: '班级作品'}
    ]
    return (
      <div styleName="tools-bar">
        <TabSwitch
          tabs={tabs}
          onClick={this.handleTabClick} />
        <Button
          style={{float: 'right', marginTop: '8px'}}
          text="新建作品"
          icon='new'
          onClick={this.handleAddClick} />
        <Button
          style={{float: 'right', marginTop: '8px', marginRight: '20px'}}
          text="提交作品"
          icon='submit' />
        <Mask show={this.state.showCourse}>
          <AddCourse
            onCloseClick={this.handleCloseCourse}
            onOpenMember={this.handleOpenMember}
            saved={this.state.savedMembers}
            delMember={this.handleDelMember} />
        </Mask>
        <Mask show={this.state.showMember}>
          <AddMember
            onCloseClick={this.handleCloseMember}
            members={this.props.members}
            savedMembers={this.state.savedMembers}
            getSaved={this.getSaved}
          />
        </Mask>
      </div>
    )
  }
}
