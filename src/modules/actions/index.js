import {createAction} from 'redux-actions'
import * as types from '../constants/actionTypes'
import axios from 'axios'

export const courseMenuGet = createAction(types.COURSEMENU_GET,
  () => axios.get('/api/v0.1/course_menu')
)

export const coursesGet = createAction(types.COURSES_GET,
  () => axios.get('/api/v0.1/courses'))

export const classCoursesGet = createAction(types.CLASSCOURSES_GET,
  () => axios.get('/api/v0.1/class_courses'))

export const membersGet = createAction(types.MEMBERS_GET,
  () => axios.get('/api/v0.1/members'))
