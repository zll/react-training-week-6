import {courseMenu} from 'modules/containers/Main/reducer'
import {courses, classCourses} from 'modules/containers/CourseContainer/reducer'
import {members} from 'modules/containers/ToolsContainer/reducer'

export default {
  courseMenu,
  courses,
  classCourses,
  members
}
